import axios from 'axios';
import { URL } from '../../../constants';
export const checkoutService = {
  getQuote: body =>
    axios({
      method: 'POST',
      url: `${URL}/api/sale`,
      data: body,
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.data.id),
  submitOrder: body =>
    axios({
      method: 'POST',
      url: `${URL}/api/sale`,
      data: body,
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(response => response.data.id)
};
