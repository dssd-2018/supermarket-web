import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import IconButton from '@material-ui/core/IconButton';

import Shopping from '../../../components/shoppingCardIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

export default class SelectCustom extends Component {
  state = {
    value: 'casa'
  };
  handleChange = () => {
    this.setState(state => ({ checked: !state.checked }));
  };
  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { checked } = this.state;
    return (
      <ListItemSecondaryAction>
        <Select
          inputProps={{
            name: 'value',
            id: 'age-simple'
          }}
          onChange={this.handleChange}
          value={this.state.value}
          style={{ width: '120px' }}
        >
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </Select>
        <IconButton aria-label="shopping_cart">
          <Shopping />
        </IconButton>
      </ListItemSecondaryAction>
    );
  }
}
