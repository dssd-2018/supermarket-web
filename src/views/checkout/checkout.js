import React, { Component } from 'react';
import Header from '../../components/header';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import './style.css';
import Loading from '../../components/loading';
export default class Checkout extends Component {
  state = {
    buying: false,
    idCoupon: '',
    username: '',
    password: ''
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  componentWillMount = () => {
    !this.props.history.location.state &&
      this.props.history.push({
        pathname: '/'
      });
    this.props.reset();
  };

  render() {
    console.log(this);
    const { username, idCoupon, password } = this.state;
    const { product, stockSelected } = this.props.history.location.state;
    return (
      <div className="container">
        {this.props.quoteProcess || this.props.buyProcess ? (
          <Loading />
        ) : (
            <div className="App">
              <Header />
              <div className="container-checkout">
                <div className="details-box">
                  <h1 className="App-title">Detalles del Producto</h1>
                  <h1 className="App-title">Producto: {product.name}</h1>
                  <h1 className="App-title">
                    Precio de lista: ${product.salePrice}{' '}
                  </h1>
                  <h1 className="App-title">
                    Cantidad Seleccionada: {stockSelected}
                  </h1>
                  <TextField
                    disabled={!!idCoupon}
                    id="username"
                    label="Nombre de usuario"
                    className="input-product-details"
                    value={this.state.username}
                    name="username"
                    onChange={this.handleChange}
                    margin="normal"
                  />
                  <TextField
                    disabled={!!idCoupon}
                    id="password-input"
                    label="Contraseña"
                    className="input-product-details"
                    name="password"
                    value={this.state.password}
                    type="password"
                    onChange={this.handleChange}
                    autoComplete="current-password"
                    margin="normal"
                  />
                  <TextField
                    disabled={!!username || !!password}
                    id="idCoupon"
                    className="input-product-details"
                    name="idCoupon"
                    label="Coupon"
                    onChange={this.handleChange}
                    value={this.state.idCoupon}
                    margin="normal"
                  />
                </div>
                {!this.props.quoteData ? (
                  <h1 className="App-title">Monto final: a cotizar </h1>
                ) : (
                    <div style={{ 'font-weight': '600' }}> <span className="App-title">
                      Monto final: ${this.props.quoteData.total}{' '} <span className="strikethroug" style={{ color: 'red' }}> ${this.props.quoteData.fullPrice}{' '}</span>
                    </span>
                    </div>

                  )}
                <div>
                  <Button
                    variant="contained"
                    className="button-coupon"
                    onClick={() =>
                      this.props.quote({
                        stockSelected: stockSelected,
                        productId: product.id,
                        ...this.state
                      })
                    }
                  >
                    Aplicar Descuento
              </Button>
                  <Button
                    variant="contained"
                    className="button-coupon"
                    onClick={() => {
                      this.props.history.push({
                        pathname: '/'
                      });
                    }}
                  >
                    Volver
              </Button>
                  {this.props.quoteData && (
                    <Button
                      onClick={() =>
                        this.props.buy({
                          stockSelected: stockSelected,
                          productId: product.id,
                          ...this.state
                        })
                      }
                      variant="contained"
                      className="listing-button">
                      Comprar
                    </Button>
                  )}
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}
