import { checkoutService } from '../services';
import { mainService } from '../../../services';
import { history } from '../../../store';
import { toast } from 'react-toastify';

const parse = (data, quote) => {
  return {
    variables: [
      {
        name: 'idProduct',
        value: data.productId
      },
      {
        name: 'quantity',
        value: data.stockSelected
      },
      !!data.username
        ? {
          name: 'username',
          value: data.username
        }
        : null,
      !!data.password
        ? {
          name: 'password',
          value: data.password
        }
        : null,
      !!data.idCoupon
        ? {
          name: 'idCoupon',
          value: data.idCoupon
        }
        : null,
      {
        name: 'quote',
        value: quote
      }
    ].filter(function (el) {
      return el != null;
    })
  };
};
export const reset = () => ({
  type: 'RESET_QUOTE'
});
export const quote = data => {
  return async (dispatch, getState) => {
    dispatch({
      type: 'GET_QUOTE'
    });
    let processId = await checkoutService.getQuote(parse(data, 'true'));
    if (!!processId) {
      var resultData = await mainService.getPullData(processId);
      console.log(resultData);
      if (resultData.status_code == 404 || resultData.status_code == 409 || resultData.status_code == 401) {
        toast.error(resultData.status_message);
      } else {
        dispatch({
          type: 'GET_QUOTE_SUCCESS',
          data: resultData
        });
      }
    } else {
      toast.error('No se pudo cotizar');
    }
    dispatch({
      type: 'GET_QUOTE_STOP'
    });
  };
};

export const buy = data => {
  return async (dispatch, getState) => {
    dispatch({
      type: 'SUBMIT_ORDER'
    });
    let processId = await checkoutService.submitOrder(parse(data, 'false'));

    if (!!processId) {
      var resultData = await mainService.getPullSubmitOrder(processId);
    } else {
      toast.toast('error');
    }

    if (resultData.eventType == 'SaleDone') {
      dispatch({
        type: 'SUBMIT_ORDER_SUCCESS',
        data: resultData
      });
      history.push({
        pathname: `/`
      });
      toast.success('compra Exitosa!');
    } else {
      if (resultData.eventType == "ErrorEvent") {
        toast.error(resultData.data.status_message);
      } else {
        toast.error("ha ocurrido un error inesperado");
      }
      dispatch({
        type: 'SUBMIT_ORDER_ERROR',
      });
    }
  };
};
