const initialState = {
  default: 'checkout',
  quote: null,
  quoteProcess: false,
  order: null,
  buyProcess: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case 'RESET_QUOTE':
      return {
        ...state,
        quote: null,
        buyProcess: false,
        order: null,
        quoteProcess: false
      };
    case 'GET_QUOTE':
      return { ...state, quote: null, quoteProcess: true };
    case 'GET_QUOTE_SUCCESS':
      return { ...state, quote: action.data, quoteProcess: false };
    case 'SUBMIT_ORDER_SUCCESS':
      return { ...state, order: action.data, buyProcess: false };
    case 'SUBMIT_ORDER_ERROR':
      return { ...state, buyProcess: false };
    case 'SUBMIT_ORDER':
      return { ...state, order: action.data, buyProcess: true };
    case 'GET_QUOTE_STOP':
      return { ...state, quoteProcess: false };
    default:
      return state;
  }
}
