import Checkout from './checkout';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { quote, reset, buy } from './redux/actions';
const mapStateToProps = state => ({
  quoteData: state.checkout.quote,
  quoteProcess: state.checkout.quoteProcess,
  buyProcess: state.checkout.buyProcess,
  order: state.checkout.order
});

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ quote, reset, buy }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Checkout);
