import React, { Component } from 'react';
import Header from '../../components/header';
import './style.css';
import Loading from '../../components/loading';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';

import ListItemText from '@material-ui/core/ListItemText';

import SelectCustom from './components/select';

export default class Listing extends Component {

  componentWillMount() {
    this.props.getProductsList();
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {

    return (
      <div className="container">
        {
          !this.props.products
            ? <Loading />
            : <AppListing {...this.props} />
        }
      </div>
    );
  }
}

class AppListing extends Component {

  render() {
    return (
      <div className="App">
        <Header />
        {this.props.error
          ? (
            <div>
              <Button variant="contained" color="primary" onClick={() => this.props.getProductsList()}>
                Reload
            </Button>
            </div>
          )
          : ""
        }
        <div className="container-products">
          <List>
            {
              this.props.products.map(prod => (
                <ListItem style={{ height: '50px' }} dense button>

                  <ListItemText
                    primary={`${prod.name}   ${'     '}    $${prod.salePrice}`} />

                  <SelectCustom
                    product={prod}
                    history={this.props.history} />

                </ListItem>
              ))
            }
          </List>
        </div>
      </div>)
  }
}
