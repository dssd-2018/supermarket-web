import mainReducer from './mainReducer';
import listing from '../views/listing/redux/reducer';
import checkout from '../views/checkout/redux/reducer';
import { reducer as formReducer } from 'redux-form';
const rootReducer = {
  mainReducer,
  listing,
  checkout,
  formReducer
};

export default rootReducer;
